"""Sort a list of formats according to their level of compression"""

import sys

# Ordered list of image formats, from lower to higher compression
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    return format1 < format2


def find_lower_pos(formats: list, pivot: int) -> int:
    lower_pos = pivot
    for i in range(pivot + 1, len(formats)):
        if lower_than(formats[i], formats[lower_pos]):
            lower_pos = i
    return lower_pos


def sort_formats(formats: list) -> list:
    for i in range(len(formats)):
        lower_pos = find_lower_pos(formats, i)
        formats[i], formats[lower_pos] = formats[lower_pos], formats[i]
    return formats


def main():
    """Read command line arguments, and print them sorted
    Also, check if they are valid formats using the fordered tuple"""

    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()


if __name__ == '__main__':
    main()